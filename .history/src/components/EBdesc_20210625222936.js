import React from 'react';
import './EBdesc.css';
import { Button } from './Button';

function EBdesc() {
    return (
        <div className="ebdesc">
            <div className="ebdesc-cat">
                <div className="ebdesc-left">
                    <img className="ebdescri" src="./cookie.png" alt="Loading..." />
                </div>
                <div className="ebdesc-right">
                    <h1>RESULTS</h1>
                    <p>Essentially Brands is an Australian owned and operated family <br/>
                    business. Armed with 50 years of experience they specialise in<br/>
                    delivering top quality products for the Australian and export market.</p><br/>
                    <p>Armed with 50 years of experience they specialise in delivering top <br/>
                    quality products for the Australian and export market.</p>
                </div>
            </div>
            <div className="ebdesc-cat2">
                <div className="ebdesc2-left">
                <p>───── NEXT CASE STUDY</p>
                    <h1>SHREE & SAMARTH</h1>
                    <p>A website for manufacturer of 100% biodegradable and <br/>
                    sustainably sourced bamboo wipes.</p>
                    <div className='project-btn'>
                        <a href="https://snsbabycare.com.au/" target="blank">
                            <Button className='btn' buttonStyle='btn--white' >View Project</Button>
                        </a>
                    </div>
                </div>
            <div className="ebdesc2-right">
                <img className="ebdesc2ri" src="./soap.png" alt="Loading..." />
            </div>
            </div>
        </div>
    )
}

export default EBdesc
