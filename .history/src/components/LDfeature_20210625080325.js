import React from 'react';
import './LDfeature.css';

function LDfeature() {
  return (
    <div className="ldfeature">
        <div className="la-feature">
            <h1>FEATURED PROJECTS</h1>
            <ul className="ldf-row1">
              <li>
                <img src="./hapii.png" alt="Loading..." />
              </li>
              <li>
                <img src="./mariquino.png" alt="Loading..." />
              </li>
              <li>
                <img src="./purpose.png" alt="Loading..." />
              </li>
            </ul>
        </div>
    </div>
  )
}

export default LDfeature
