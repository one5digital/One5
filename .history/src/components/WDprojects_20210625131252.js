import React from 'react';
import './WDprojects.css';
import { Link } from 'react-router-dom';
 
function WDprojects() {
    return (
        <div className="wdproject">
            <div>
                <h1>FEATURED PROJECTS </h1>
            </div>
            <div className="wdpro-wrapper">
                <div className="wdprorow">
                    <ul className="wdprorow-items">
                        <li>
                            <Link exact to="/Projects/EssentiallyBrands">
                            <img className="wdproimg" src="./essentiallybrands.png" alt="Loading..." /></Link>
                        </li>
                        <li>
                            <img className="wdproimg" src="./accord.png" alt="Loading..." />
                        </li>
                        <li>
                            <img className="wdproimg" src="./tiff.png" alt="Loading..." />
                        </li>
                        <li>
                            <img className="wdproimg" src="./aus.png" alt="Loading..." />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default WDprojects
