import React from 'react';
import './SEOplans.css';
import { Button } from './Button';
import { FiCheckCircle } from "react-icons/fi";

function SEOplans() {
    return (
        <div className="seoplan">
                <div className="seoplan-desc">
                    <h1>OUR PLANS</h1>
                    <ul className="seoplanp">
                        <li>
                            <div className="seoplan-col1">
                                <h4 className="seoplanb">BASIC</h4>
                                <p><FiCheckCircle className="check"/> Instant Design Concept</p>
                                <p><FiCheckCircle className="check"/> Logo Transparency</p>
                                <p><FiCheckCircle className="check"/> Basic Design</p>
                                <p><FiCheckCircle className="check"/> Text Based Logo</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <div className='seoplanbasic-btn'>
                                    <Button className='btn' buttonStyle='btn--orange'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="seoplan-col2">
                                <h4 className="seoplanw">STANDRED</h4>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> Instant Design Concept</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> Logo Transparency</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> Basic Design</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> Text Based Logo</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> One Revision</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> JPG File</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> One Revision</p>
                                <p className="seoplanw"><FiCheckCircle className="checkw"/> JPG File</p>
                                <div className='seoplanstandred-btn'>
                                    <Button className='btn' buttonStyle='btn--owhite' >GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="seoplan-col3">
                                <h4 className="seoplanb">PREMIUM</h4>
                                <p><FiCheckCircle className="check"/> Instant Design Concept</p>
                                <p><FiCheckCircle className="check"/> Logo Transparency</p>
                                <p><FiCheckCircle className="check"/> Basic Design</p>
                                <p><FiCheckCircle className="check"/> Text Based Logo</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <div className='seoplanpremium-btn'>
                                    <Button className='btn' buttonStyle='btn--orange'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
           </div>
    )
}

export default SEOplans
