import React from 'react';
import './EBdesc.css';

function EBdesc() {
    return (
        <div>
            <div className="ebdesc-cat">
                <div className="ebdesc-left">
                    <h1>CATALOGUE</h1>
                    <p>Armed with 50 years of experience they specialise in <br/>
                    delivering top quality products for the Australian and <br/>
                    export market.</p>
                </div>
                <div className="ebdesc-right">
                    <img className="ebdescri" src="./eb-cat.png" alt="Loading..." />
                </div>
            </div>
        </div>
    )
}

export default EBdesc
