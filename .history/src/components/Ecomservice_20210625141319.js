import React from 'react';
import './Ecomservice.css';

function Ecomservice() {
    return (
        <div className="ecservice">
        <div>
            <h1>OUR ECOMMERCE SERVICES</h1>
        </div>
        <div className="ecservice-wrapper">
            <ul className="ecservice-1">
                <h2 className="ecno">01</h2>
                <h6>ECOMMERCE WEBSITE <br/>DESIGN </h6>
                <p className="ec-desc">A custom website design <br/> provides you with a trustworthy <br/> digital presence that meets your <br/> business needs in terms of <br/> quality, branding and usability.
                </p>
            </ul>
            <ul className="ecservice-2">
                <h2 className="ecno">02</h2>
                <h6>INTEGRATION, UPGRADE, <br/>AND OPTIMIZATION</h6>
                <p className="ec-desc">Ensure your website adapts to all <br/> screen sizes and devices to <br/> increase your customer retention. </p>
            </ul>
            <ul className="ecservice-3">
                <h2 className="ecno">03</h2>
                <h6>WEBSITE PERFORMANCE <br/>ANALYSIS </h6>
                <p className="ec-desc">Increase your leads and <br/> conversions with One5 Digital <br/> custom website design <br/> packages. </p>
            </ul>
            <ul className="ecservice-4">
                <h2 className="ecno">04</h2>
                <h6>ONGOING SUPPORT AND <br/>MAINTENANCE </h6>
                <p className="ec-desc">Our web hosting services ensure <br/> high reliability and uptime, site <br/> security and improved SEO and <br/> online performance. </p>
            </ul>
        </div>
    </div> 
    )
}

export default Ecomservice
