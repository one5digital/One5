import React from 'react';
import './LDpricing.css';
import { Button } from './Button';
import { IconName } from "react-icons/fi";

function LDpricing() {
    return (
        <div className="ldprice">
            <div className="ld-price">
                <div className="ldf-desc">
                    <h1>OUR PRICING PLANS</h1>
                    <ul className="ldfp">
                        <li>
                            <h2 className="ldfw">BASIC</h2>
                            <div className='ldprice-btn'>
                                <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                            </div>
                        </li>
                        <li>
                            <h2 className="ldfw">STANDRED</h2>
                            <div className='ldprice-btn'>
                                <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                            </div>
                        </li>
                        <li>
                            <h2 className="ldfw">PREMIUM</h2>
                            <div className='ldprice-btn'>
                                <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                            </div>
                        </li>
                    </ul>
                </div>
           </div>
        </div>
    )
}

export default LDpricing
