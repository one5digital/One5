import React from 'react';
import './EBpro.css';

function EBpro() {
    return (
        <div className="ebpro">
            <div>
                <h1>HOMEPAGE</h1>
                <p>Armed with 50 years of experience they specialise in delivering top quality <br/>
                product for the Australian and export market.</p>
            </div>
            <div>
                <img className="ebi" src="./ebgif.gif" alt="Loading..."/>
            </div>
            <div className="eb-cat">
                <div className="eb-left">
                    <h1>CATALOGUE</h1>
                    <p>Armed with 50 years of experience they specialise in <br/>
                    delivering top quality products for the Australian and <br/>
                    export market.</p>
                </div>
                <div className="eb-right">
                    <img className="ebri" src="./eb-cat.png" alt="Loading..." />
                </div>
            </div>
        </div>
    )
}

export default EBpro
