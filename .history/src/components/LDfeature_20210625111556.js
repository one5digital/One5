import React from 'react';
import './LDfeature.css';

function LDfeature() {
  return (
    <div className="ldfeature">
        <div className="la-feature">
            <h1>FEATURED PROJECTS</h1>
            <ul className="ldf-row1">
              <li>
                <img className="ldfi" src="./hapii.png" alt="Loading..." />
              </li>
              <li>
                <img className="ldfi" src="./mariquino.png" alt="Loading..." />
              </li>
              <li>
                <img className="ldfi" src="./purpose.png" alt="Loading..." />
              </li>
            </ul>
            <ul className="ldf-row2">
              <li>
                <img className="ldfi" src="./oilpaint.png" alt="Loading..." />
              </li>
              <li>
                <img className="ldfi" src="./beaut.png" alt="Loading..." />
              </li>
              <li>
                <img className="ldfi" src="./spice.png" alt="Loading..." />
              </li>
            </ul>
        </div>
    </div>
  )
}

export default LDfeature
