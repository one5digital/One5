import React from 'react';
import './LDpricing.css';
import { Button } from './Button';
import { IconName } from "react-icons/fi";

function LDpricing() {
    return (
        <div className="ldprice">
            <div className="ld-price">
                <div className="ldf-desc">
                    <h1>OUR PRICING PLANS</h1>
                    
                    <div className='ldprice-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                    </div>
                </div>
           </div>
        </div>
    )
}

export default LDpricing
