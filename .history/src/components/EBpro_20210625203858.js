import React from 'react';
import './EBpro.css';

function EBpro() {
    return (
        <div className="ebpro">
            <div>
                <h1>MORE ABOUT PROJECT</h1>
                <p>Essentially Brands is an Australian owned and operated family business. Armed <br/> with 50 years of experience they specialise in delivering top quality 
                product for the <br/> Australian and export market.</p>
            </div>
            <div className="ebpro-slider">
                <img className="slide1" src="./slide1.png" alt="Loading..."/>
                <img className="slide2" src="./slide2a.png" alt="Loading..."/>
                <img className="slide3" src="./slide3.png" alt="Loading..."/>
            </div>
        </div>
    )
}

export default EBpro
