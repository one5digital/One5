import React from 'react';
import './SEOsteps.css';

function SEOsteps() {
    return (
        <div className="seosteps">
            <div className="seosteps-desc">
                <div className="seosteps-left">
                    <h1>SEO STEPS & STRATEGY</h1>
                </div>
                <div className="seosteps-right">
                    <ul className="seosteps-list scrollbar-warning">
                        <li className="seosteps-items">
                            <h4><span style={{color: '#F04E31'}} >01.</span> ON PAGE OPTIMIZATION</h4>
                            <li className="seosteps-items">Improve tracking and control over inventory activities and stock movements, take control over your inventory.</li>
                        </li>
                        <li className="seosteps-items">
                            <h4><span style={{color: '#F04E31'}} >02.</span> BUSINESS ANALYTICS</h4>
                            <p className="item-2">Our team perform a competitive analysis, and begin to map out the <br/> strategy.</p>
                        </li>
                        <li className="seosteps-items">
                            <h4><span style={{color: '#F04E31'}} >03.</span> DESIGN & PROTOTYPING</h4>
                            <p className="item-3">We create the actual conceptualizing of the solution, that is the <br/> detailed software architecture meeting specific project requirements  <br/> is created.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default SEOsteps
