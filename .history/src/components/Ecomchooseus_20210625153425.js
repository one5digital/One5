import React from 'react';
import './Ecomchooseus.css';
import { Link } from 'react-router-dom';

function Ecomchooseus() {
    return (
        <div className="ecomus">
            <div>
                <h1>WHY CHOOSE ONE5 DIGITAL</h1>
                <p>We are a full service digital agency so we provide a range of strategic, creative <br/>
                and technical solutions.</p>
            </div>
            <div className="ecomus-wrapper">
                <ul className="ecomus-1">
                    <div>
                        <img src= "./computer.png" alt= "Loading..." />
                        <h6>WE ARE DEVELOPERS<br/> & DIGITAL MARKETER</h6>
                    </div>
                </ul>
                <ul className="ecomus-2">
                    <div>
                        <img src= "./practice.png" alt= "Loading..." />
                        <h6>10+ YEARS<br/> OF EXPERIENCE</h6>
                    </div>
                </ul>
                <ul className="ecomus-3">
                    <div>
                        <img src= "./group.png" alt= "Loading..." />
                        <h6>WE WORK WITH<br/> DIFFERENT PLATFORMS</h6>
                    </div>
                </ul>
                <ul className="ecomus-4">
                    <div>
                        <img src= "./online-shop.png" alt= "Loading..." />
                        <h6>EXPERTS<br/> IN ECOMMERCE</h6>
                    </div>
                </ul>
            </div>
            <div className="ecousproject">
                <div>
                    <h1>FEATURED PROJECTS </h1>
                </div>
                <div className="ecous-wrapper">
                    <div className="ecousrow">
                        <ul className="ecousrow-items">
                            <li>
                                <Link exact to="/Projects/EssentiallyBrands">
                                <img className="ecousimg" src="./essentiallybrands.png" alt="Loading..." /></Link>
                            </li>
                            <li>
                                <img className="ecousimg" src="./accord.png" alt="Loading..." />
                            </li>
                            <li>
                                <img className="ecousimg" src="./tiff.png" alt="Loading..." />
                            </li>
                            <li>
                                <img className="ecousimg" src="./aus.png" alt="Loading..." />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Ecomchooseus
