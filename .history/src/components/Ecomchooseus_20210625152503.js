import React from 'react';
import './Ecomchooseus.css';

function Ecomchooseus() {
    return (
        <div className="ecomus">
            <div>
                <h1>WHY CHOOSE ONE5 DIGITAL</h1>
                <p>We are a full service digital agency so we provide a range of strategic, creative <br/>
                and technical solutions.</p>
            </div>
            <div className="ecomus-wrapper">
                <ul className="ecomus-1">
                    <div>
                        <img src= "./computer.png" alt= "Loading..." />
                        <h6>OPTIMISED<br/> BUSINESS PROCESS</h6>
                    </div>
                </ul>
                <ul className="ecomus-2">
                    <div>
                        <img src= "./practice.png" alt= "Loading..." />
                        <h6>FULL<br/> OWNERSHIP</h6>
                    </div>
                </ul>
                <ul className="ecomus-3">
                    <div>
                        <img src= "./group.png" alt= "Loading..." />
                        <h6>FULL<br/> FLEXIBILITY</h6>
                    </div>
                </ul>
                <ul className="ecomus-4">
                    <div>
                        <img src= "./online-shop.png" alt= "Loading..." />
                        <h6>FAST<br/> INTEGRATION</h6>
                    </div>
                </ul>
            </div>
        </div>
    )
}

export default Ecomchooseus
