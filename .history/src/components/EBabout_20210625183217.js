/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button } from './Button';
import './EBabout.css';
import { Link } from 'react-router-dom';

function EBabout() {
    
    const [button, setButton] = useState(true);
        
    const showButton = () => {
        if(window.innerWidth <= 960) {
            setButton(false);
        } else {
            setButton(true);
        }
    };
    
    useEffect ( ( ) => {
        showButton();
    }, []);

    window.addEventListener('resize', showButton);

    return (
        <div className = 'ebabout'>
            <div className = 'ebabout-container'>
                <div className = 'ebaboutcol-left'>
                    <Link exact to="/Services"><p>< img src="../Arrow2.png" alt="Loading..." / > BACK TO PROJECTS</p></Link>
                    <h1>ESSENTIALLY BRANDS</h1>
                    <h3>ABOUT CLIENT</h3>
                    <p className = 'ebabout-desc'>Essentially Brands is an Australian owned and operated family<br/>
                    business. Armed with 50 years of experience they specialise in<br/>
                    delivering top quality products for the Australian and export market.</p>
                    <h3>SERVICE</h3>
                    <div className="ser-wrapper">
                        <ul className="ser-1">
                            <li>Digital Strategy</li>
                            <li>Web Design</li>
                            <li>Branding</li>
                        </ul>
                        <ul className="ser-2">
                            <li>Development</li>
                            <li>User Experience</li>
                            <li>Enterprise UX</li>
                        </ul>
                    </div>
                    <h3>YEAR</h3>
                    <div className='ebabout-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>Visit Website</Button>
                    </div>
                </div>
                <div className = 'ebaboutcol-right'>
                    <img className="ebimg" src= "./eb.png" alt= "Loading..." />
                </div>
            </div>
        </div>
    )
}

export default EBabout
