import React from 'react';
import './Ecomservice.css';

function Ecomservice() {
    return (
        <div className="ecservice">
        <div>
            <h1>OUR ECOMMERCE SERVICES</h1>
        </div>
        <div className="ecservice-wrapper">
            <ul className="ecservice-1">
                <h1 className="ecno">01</h1>
                <h6>ECOMMERCE WEBSITE <br/>DESIGN </h6>
                <p className="ec-desc">Our Ecommerce services bring <br/> value to your business by <br/> delivering the right custom <br/> solution that your business <br/> demands.
                </p>
            </ul>
            <ul className="ecservice-2">
                <h1 className="ecno">02</h1>
                <h6>INTEGRATION, UPGRADE, <br/>AND OPTIMIZATION</h6>
                <p className="ec-desc">With our experience in delivering <br/> what the client needs, we can <br/> build your Ecommerce website <br/> with no hassle.</p>
            </ul>
            <ul className="ecservice-3">
                <h1 className="ecno">03</h1>
                <h6>WEBSITE PERFORMANCE <br/>ANALYSIS </h6>
                <p className="ec-desc">We make sure your website <br/> displays the most important <br/> content as quickly as possible <br/> and don’t suffer unexpected <br/> downtimes.</p>
            </ul>
            <ul className="ecservice-4">
                <h1 className="ecno">04</h1>
                <h6>ONGOING SUPPORT AND <br/>MAINTENANCE </h6>
                <p className="ec-desc">To keep your eCommerce <br/> website running smoothly we <br/> offer complete maintenance and <br/> on-demand support to grow your <br/> business.</p>
            </ul>
        </div>
        <div className="ecdesc">
            <div className="ecleft-desc">
                <h1>WE BUILD SOLUTIONS<br/> 
                WITH <span style={{color: '#F04E31'}} >CREATIVE </span> DESIGN</h1>
            </div>
            <div className="ecright-desc">
                <p className="ecproject-desc">Client success is measured by result.<br/> We have the great team to make <br/> an outstanding results.</p>
            </div>
        </div>
    </div> 
    )
}

export default Ecomservice
