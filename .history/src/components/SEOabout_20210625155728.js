import React from 'react';
import './SEOabout.css';
import { Link } from 'react-router-dom';
import { Button } from './Button';

function SEOabout() {
    return (
        <div className="wdabout">
            <div className="seo-about">
                <Link exact to="/Services"><p>< img src="../Arrow.png" alt="Loading..." / > BACK TO SERVICES</p></Link>
                <div className="seo-desc">
                    <h1>WEB DESIGN</h1>
                    <p>When we build you a website, not only are we responsible for you a modern ,<br/> updated 
                        we are responsible for making that website a sales tool for your <br/> business. </p><br/>
                    <p>Through intense research and measurement, we design your site so that causal <br/> visitors become your customers. </p>
                    <div className='seoabout-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>GET A QUOTE</Button>
                    </div>
                </div>
           </div>
        </div>
    )
}

export default SEOabout
