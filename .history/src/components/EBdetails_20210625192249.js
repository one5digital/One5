import React from 'react';
import './EBdetails.css';

function EBdetails() {
    return (
        <div className="ebdetails">
            <div className="left-ebdetails">
                <h1>A MORDEN DIGITAL PRESENCE<br/>
                FOR THE WHOLESALER<br/>
                OF PET PRODUCTS</h1>
                <p>Essentially Brands is an Australian owned and <br/>operated family business. Armed with 50 years of<br/> experience they specialise in delivering top quality <br/>
                products for the Australian and export market. </p>
            </div>
        </div>
    )
}

export default EBdetails
