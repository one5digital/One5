import React from 'react';
import './PPCabout.css';
import { Link } from 'react-router-dom';
import { Button } from './Button';

function PPCabout() {
    return (
        <div className="ppcabout">
            <div className="ppc-about">
                <Link exact to="/Services"><p>< img src="../Arrow.png" alt="Loading..." / > BACK TO SERVICES</p></Link>
                <div className="ppc-desc">
                    <h1>SEARCH ENGINE OPTIMIZATION (ppc)</h1>
                    <p>A word that we all have heard, but very few understand the process to deliver the <br/> best results.
                    The organic ppc is the best approach to any company looking for a <br/> long term investment with fruitful return.</p><br/>
                    <p>We will help drive more business to your website to sell more of your products and <br/>services! Start utilizing online marketing strategies that work.
                    </p><br/>
                    <p>Below are some steps to deliver the results.
                    </p>
                    <div className='ppcabout-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>GET A QUOTE</Button>
                    </div>
                </div>
           </div>
        </div>
    )
}

export default PPCabout
