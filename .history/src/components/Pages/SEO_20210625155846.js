import React from 'react';
import SEOabout from '../SEOabout';

function SEO() {
    return (
        <React.Fragment> 
            <SEOabout />
        </React.Fragment>
    )
}

export default SEO
