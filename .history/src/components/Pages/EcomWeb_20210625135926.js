import React from 'react';
import Ecomabout from '../Ecomabout';
import Getintouch from '../Getintouch';


function EcomWeb() {
    return (
        <React.Fragment> 
            <Ecomabout />
            <Getintouch />
        </React.Fragment>
    )
}

export default EcomWeb
