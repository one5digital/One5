import React from 'react';
import Ecomabout from '../Ecomabout';
import Ecomservice from '../Ecomservice';
import Ecomchooseus from '../Ecomchooseus';
import Getintouch from '../Getintouch';

function EcomWeb() {
    return (
        <React.Fragment> 
            <Ecomabout />
            <Ecomservice />
            <Ecomchooseus />
            <Getintouch />
        </React.Fragment>
    )
}

export default EcomWeb
