import React from 'react';
import PPCabout from '../PPCabout';
import Getintouch from '../Getintouch';

function PPC() {
    return (
        <React.Fragment> 
            <PPCabout />
            <Getintouch />
        </React.Fragment>
    )
}

export default PPC
