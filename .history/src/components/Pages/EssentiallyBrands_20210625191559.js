import React from 'react';
import EBabout from '../EBabout';
import EBdetails from '../EBdetails';

function EssentiallyBrands() {
    return (
        <React.Fragment> 
            <EBabout />
            <EBdetails />
        </React.Fragment>
    )
}

export default EssentiallyBrands
