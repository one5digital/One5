import React from 'react';
import SEOabout from '../SEOabout';
import SEOsteps from '../SEOsteps';
import SEOplans from '../SEOplans';

function SEO() {
    return (
        <React.Fragment> 
            <SEOabout />
            <SEOsteps />
            <SEOplans />
        </React.Fragment>
    )
}

export default SEO
