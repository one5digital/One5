import React from 'react';
import SEOabout from '../SEOabout';
import SEOsteps from '../SEOsteps';

function SEO() {
    return (
        <React.Fragment> 
            <SEOabout />
            <SEOsteps />
        </React.Fragment>
    )
}

export default SEO
