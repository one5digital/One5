import React from 'react';
import EBabout from '../EBabout';
import EBdetails from '../EBdetails';
import EBexp from '../EBexp';
import EBpro from '../EBpro';

function EssentiallyBrands() {
    return (
        <React.Fragment> 
            <EBabout />
            <EBdetails />
            <EBexp />
            <EBpro />
        </React.Fragment>
    )
}

export default EssentiallyBrands
