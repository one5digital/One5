import React from 'react';
import WDabout from '../WDabout';
import Getintouch from '../Getintouch';


function WebDesign() {
    return (
        <React.Fragment> 
            <WDabout />
            <Getintouch />
        </React.Fragment>
    )
}

export default WebDesign
