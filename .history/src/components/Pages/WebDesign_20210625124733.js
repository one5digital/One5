import React from 'react';
import WDabout from '../WDabout';
import WDservice from '../WDservice';
import Getintouch from '../Getintouch';


function WebDesign() {
    return (
        <React.Fragment> 
            <WDabout />
            <WDservice />
            <Getintouch />
        </React.Fragment>
    )
}

export default WebDesign
