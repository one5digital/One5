import React from 'react';
import Ecomabout from '../Ecomabout';
import Ecomservice from '../Ecomservice';
import Getintouch from '../Getintouch';


function EcomWeb() {
    return (
        <React.Fragment> 
            <Ecomabout />
            <Ecomservice />
            <Getintouch />
        </React.Fragment>
    )
}

export default EcomWeb
