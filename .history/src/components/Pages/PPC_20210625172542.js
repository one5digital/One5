import React from 'react';
import PPCabout from '../PPCabout';
import PPCsteps from '../PPCsteps';
import Getintouch from '../Getintouch';

function PPC() {
    return (
        <React.Fragment> 
            <PPCabout />
            <PPCsteps />
            <Getintouch />
        </React.Fragment>
    )
}

export default PPC
