import React from 'react';
import EBabout from '../EBabout';
import EBdetails from '../EBdetails';
import EBexp from '../EBexp';

function EssentiallyBrands() {
    return (
        <React.Fragment> 
            <EBabout />
            <EBdetails />
            <EBexp />
        </React.Fragment>
    )
}

export default EssentiallyBrands
