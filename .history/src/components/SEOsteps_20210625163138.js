import React from 'react';
import './SEOsteps.css';

function SEOsteps() {
    return (
        <div className="seosteps">
            <div className="seosteps-desc">
                <div className="seosteps-left">
                    <h1>SEO STEPS & STRATEGY</h1>
                </div>
                <div className="seosteps-right">
                    <ul className="seosteps-list scrollbar-warning">
                        <li className="seosteps-items">
                            <h4><span style={{color: '#F04E31'}} >01.</span> ON PAGE OPTIMIZATION</h4>
                            <li className="seosteps-item">Keywords</li>
                            <li className="seosteps-item">Meta Tags Optimization</li>
                            <li className="seosteps-item">Homepage Content</li>
                            <li className="seosteps-item">Keyword rich content (Landing Pages)</li>
                            <li className="seosteps-item">Robots.txt Optimization</li>
                            <li className="seosteps-item">Adding Keyword rich alt text to images</li>
                            <li className="seosteps-item">Adding keyword rich titles to hyperlinks</li>
                            <li className="seosteps-item">Adding heading tags (H1...H6)</li>
                            <li className="seosteps-item">GEO Sitemap & XML file</li>
                            <li className="seosteps-item">Updating Xml Sitemap</li>
                            <li className="seosteps-item">Updating simple html sitemap</li>
                            <li className="seosteps-item">Canonicalization</li>
                            <li className="seosteps-item">Installing Google analytics Code</li>
                        </li>
                        <li className="seosteps-items">
                            <h4><span style={{color: '#F04E31'}} >02.</span> Local Search Engine Optimization</h4>
                            <li className="seosteps-item">Local Search Engine submission</li>
                            <li className="seosteps-item">Meta Tags Optimization</li>
                            <li className="seosteps-item">Schema Integration</li>
                            <li className="seosteps-item">Updating Pages for Local Search (adding Local address and <br/>testimonials)</li>
                            <li className="seosteps-item">Local Directory submission</li>
                            <li className="seosteps-item">Customer Review submission to local directories</li>
                        </li>
                        <li className="seosteps-items">
                            <h4><span style={{color: '#F04E31'}} >03.</span> DESIGN & PROTOTYPING</h4>
                            <p className="item-3">We create the actual conceptualizing of the solution, that is the <br/> detailed software architecture meeting specific project requirements  <br/> is created.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default SEOsteps
