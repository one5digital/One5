/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button } from './Button';
import './EBabout.css';
import { Link } from 'react-router-dom';

function EBabout() {
    
    const [button, setButton] = useState(true);
        
    const showButton = () => {
        if(window.innerWidth <= 960) {
            setButton(false);
        } else {
            setButton(true);
        }
    };
    
    useEffect ( ( ) => {
        showButton();
    }, []);

    window.addEventListener('resize', showButton);

    return (
        <div className = 'ebabout'>
            <div className = 'ebabout-container'>
                <div className = 'ebaboutcol-left'>
                    <Link exact to="/Services"><p>< img src="../Arrow.png" alt="Loading..." / > BACK TO PROJECTS</p></Link>
                    <p className = 'ebabout-shortdesc' style={{ color: '#F04E31' }}>───── WE ARE</p>
                    <h1>A FULL SERVICE <br/>
                    DIGITAL AGENCY</h1>
                    <p className = 'ebabout-desc'>Our creative team is focused on stunning and results driven <br/>
                    solutions for small to medium business all over the world.</p>
                    <div className='ebabout-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>Visit Website</Button>
                    </div>
                </div>
                <div className = 'ebaboutcol-right'>
                    <img className="ebimg" src= "./eb.png" alt= "Loading..." />
                </div>
            </div>
        </div>
    )
}

export default EBabout
