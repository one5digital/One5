import React from 'react';
import './LDpricing.css';
import { Button } from './Button';
import { IconName } from "react-icons/fi";

function LDpricing() {
    return (
        <div className="ldprice">
            <div className="ld-price">
                <div className="ldf-desc">
                    <h1>OUR PRICING PLANS</h1>
                    <p className="ld-price">Logo design is the utmost important start for any business. It dictates everything <br/>
                    from your message, business card, brochure, marketing materials, website, online<br/>
                    marketing.</p><br/>
                    <p className="ld-price">We craft meaningful brands through visual identity, print and digital experience for <br/>
                    small to medium business all over the world.</p>
                    <div className='ldprice-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>GET A QUOTE</Button>
                    </div>
                </div>
           </div>
        </div>
    )
}

export default LDpricing
