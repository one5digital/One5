import React from 'react';
import './EBpro.css';

function EBpro() {
    return (
        <div className="ebpro">
            <div>
                <h1>MORE ABOUT PROJECT</h1>
                <p>Essentially Brands is an Australian owned and operated family business. Armed <br/> with 50 years of experience they specialise in delivering top quality 
                product for the <br/> Australian and export market.</p>
            </div>
            <div className="ebpro-slider">
                <img className="slide1" src="./eb-slide1.png" alt="Loading..."/>
                <img className="slide2" src="./eb-slide2a.png" alt="Loading..."/>
                <img className="slide3" src="./eb-slide3.png" alt="Loading..."/>
            </div>
            <p>Product Catalogue</p>
            <div className="page-arrows">
                <i class="fas fa-arrow-left"></i> 01 - 03 <i class="fas fa-arrow-right"></i>
            </div>
        </div>
    )
}

export default EBpro
