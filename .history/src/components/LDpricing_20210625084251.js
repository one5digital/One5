import React from 'react';
import './LDpricing.css';
import { Button } from './Button';
import { IconName } from "react-icons/fi";

function LDpricing() {
    return (
        <div className="ldprice">
            <div className="ld-price">
                <div className="ldf-desc">
                    <h1>OUR PRICING PLANS</h1>
                    <ul className="ldfp">
                        <li>
                            <div className="ldf-col1">
                                <h4 className="ldfw">BASIC</h4>
                                <div className='ldprice-btn'>
                                    <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <div className="ldf-col2">
                                <h4 className="ldfw">STANDRED</h4>
                                <div className='ldprice-btn'>
                                    <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <div className="ldf-col3">
                                <h4 className="ldfw">PREMIUM</h4>
                                <div className='ldprice-btn'>
                                    <Button className='btn' buttonStyle='btn--primary'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
           </div>
        </div>
    )
}

export default LDpricing
