import React from 'react';
import './PPCsteps.css';

function PPCsteps() {
    return (
        <div className="ppcsteps">
            <div className="ppc-desc">
                <div className="ppc-left">
                    <h1>DID YOU KNOW, THAT <br/>
                    WITH PPC YOU CAN</h1>
                </div>
                <div className="ppc-right">
                    <ul className="ppc-list scrollbar-warning">
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >01.</span> IDEA</h4>
                        </li>
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >02.</span> BUSINESS ANALYTICS</h4>
                        </li>
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >03.</span> DESIGN & PROTOTYPING</h4>
                        </li>
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >04.</span> ppcELOPMENT</h4>
                        </li>
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >05.</span> TESTING</h4>
                        </li>
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >06.</span> DEPLOYMENT</h4>
                        </li>
                        <li className="ppc-items">
                            <h4><span style={{color: '#F04E31'}} >07.</span> MAINTENANCE </h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default PPCsteps
