import React from 'react';
import './EBdesc.css';

function EBdesc() {
    return (
        <div className="ebdesc">
            <div className="ebdesc-cat">
                <div className="ebdesc-left">
                    <img className="ebdescri" src="./eb-cat.png" alt="Loading..." />
                </div>
                <div className="ebdesc-right">
                    <h1>RESULTS</h1>
                    <p>Essentially Brands is an Australian owned and operated family <br/>
                    business. Armed with 50 years of experience they specialise in<br/>
                    delivering top quality products for the Australian and export market.</p><br/>
                    <p>Armed with 50 years of experience they specialise in delivering top <br/>
                    quality products for the Australian and export market.</p>
                </div>
            </div>
            <div className="ebdesc-cat2">
                <div className="ebdesc2-left">
                    <h1>CATALOGUE</h1>
                    <p>Armed with 50 years of experience they specialise in <br/>
                    delivering top quality products for the Australian and <br/>
                    export market.</p>
                </div>
            </div>
            <div className="ebdesc2-right">
                <img className="ebdesc2ri" src="./eb-cat.png" alt="Loading..." />
            </div>
        </div>
    )
}

export default EBdesc
