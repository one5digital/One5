import React from 'react';
import './LDpricing.css';
import { Button } from './Button';
import { FiCheckCircle } from "react-icons/fi";

function SEOplans() {
    return (
        <div className="ldprice">
            <div className="ld-price">
                <div className="ldf-desc">
                    <h1>OUR PRICING PLANS</h1>
                    <ul className="ldfp">
                        <li>
                            <div className="ldf-col1">
                                <h4 className="ldfb">BASIC</h4>
                                <p><FiCheckCircle className="check"/> Instant Design Concept</p>
                                <p><FiCheckCircle className="check"/> Logo Transparency</p>
                                <p><FiCheckCircle className="check"/> Basic Design</p>
                                <p><FiCheckCircle className="check"/> Text Based Logo</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <div className='ldbasic-btn'>
                                    <Button className='btn' buttonStyle='btn--orange'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="ldf-col2">
                                <h4 className="ldfw">STANDRED</h4>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> Instant Design Concept</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> Logo Transparency</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> Basic Design</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> Text Based Logo</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> One Revision</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> JPG File</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> One Revision</p>
                                <p className="ldfw"><FiCheckCircle className="checkw"/> JPG File</p>
                                <div className='ldstandred-btn'>
                                    <Button className='btn' buttonStyle='btn--owhite' >GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="ldf-col3">
                                <h4 className="ldfb">PREMIUM</h4>
                                <p><FiCheckCircle className="check"/> Instant Design Concept</p>
                                <p><FiCheckCircle className="check"/> Logo Transparency</p>
                                <p><FiCheckCircle className="check"/> Basic Design</p>
                                <p><FiCheckCircle className="check"/> Text Based Logo</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <p><FiCheckCircle className="check"/> One Revision</p>
                                <p><FiCheckCircle className="check"/> JPG File</p>
                                <div className='ldpremium-btn'>
                                    <Button className='btn' buttonStyle='btn--orange'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
           </div>
        </div>
    )
}

export default SEOplans
