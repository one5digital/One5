import React from 'react';
import { Link } from 'react-router-dom';
 
function WDprojects() {
    return (
        <div>
            <div>
                <h1>FEATURED PROJECTS </h1>
            </div>
            <div className="pro-wrapper">
                <div className="prorow-1">
                    <ul className="prorow1-items">
                        <li>
                            <Link exact to="/Projects/EssentiallyBrands">
                            <img className="proimg" src="./essentiallybrands.png" alt="Loading..." /></Link>
                            <h6>PROJECT:<p>ESSENTIALLY BRANDS</p></h6>
                        </li>
                        <li>
                            <img className="proimg" src="./accord.png" alt="Loading..." />
                            <h6>PROJECT:<p>ACCORD RETAIL</p></h6>
                        </li>
                        <li>
                            <img className="proimg" src="./tiff.png" alt="Loading..." />
                            <h6>PROJECT:<p>TIFFANY SHEN</p></h6>
                        </li>
                        <li>
                            <img className="proimg" src="./aus.png" alt="Loading..." />
                            <h6>PROJECT:<p>AUSPORT</p></h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default WDprojects
