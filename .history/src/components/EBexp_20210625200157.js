import React from 'react';
import './EBexp.css';

function EBexp() {
    return (
        <div className="ebexp">
            <div>
                <h1>HOMEPAGE</h1>
                <p>Armed with 50 years of experience they specialise in delivering top quality <br/>
                product for the Australian and export market.</p>
            </div>
            <div>
                <img className="ebi" src="./ebgif.gif" alt="Loading..."/>
            </div>
            <div className="eb-left">

            </div>
        </div>
    )
}

export default EBexp
