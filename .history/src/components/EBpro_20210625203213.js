import React from 'react';
import './EBpro.css';

function EBpro() {
    return (
        <div className="ebpro">
            <div>
                <h1>HOMEPAGE</h1>
                <p>Armed with 50 years of experience they specialise in delivering top quality <br/>
                product for the Australian and export market.</p>
            </div>
        </div>
    )
}

export default EBpro
