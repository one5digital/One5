/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Button } from './Button';
import './EBabout.css';

function EBabout() {
    
    const [button, setButton] = useState(true);
        
    const showButton = () => {
        if(window.innerWidth <= 960) {
            setButton(false);
        } else {
            setButton(true);
        }
    };
    
    useEffect ( ( ) => {
        showButton();
    }, []);

    window.addEventListener('resize', showButton);

    return (
        <div className = 'hero-section'>
            <div className = 'hero-container'>
                <div className = 'col-left'>
                    <p className = 'hero-shortdesc' style={{ color: '#F04E31' }}>───── WE ARE</p>
                    <h1>A FULL SERVICE <br/>
                    DIGITAL AGENCY</h1>
                    <p className = 'hero-desc'>Our creative team is focused on stunning and results driven <br/>
                    solutions for small to medium business all over the world.</p>
                    <div className='hero-btn'>
                        <Button className='btn' buttonStyle='btn--primary'>Visit Website</Button>
                    </div>
                </div>
                <div className = 'col-right'>
                    <img src= "./blob.gif" alt= "Loading..." />
                </div>
            </div>
        </div>
    )
}

export default EBabout
