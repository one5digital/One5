import React from 'react';
import './SEOplans.css';
import { Button } from './Button';
import { FiCheckCircle } from "react-icons/fi";

function SEOplans() {
    return (
        <div className="seoplan">
                <div className="seoplan-desc">
                    <h1>OUR PLANS</h1>
                    <ul className="seoplanp">
                        <li>
                            <div className="seoplan-col1">
                                <h4 className="seoplanb">BASIC START UP</h4>
                                <h4 className="seorange">10</h4>
                                <p className="seorange">Keywords</p>
                                <p><FiCheckCircle className="check"/> On Page Optimization</p>
                                <p><FiCheckCircle className="check"/> 24/7 Keyword Analysis</p>
                                <p><FiCheckCircle className="check"/> Link Building</p>
                                <p><FiCheckCircle className="check"/> Google/Yahoo/Bing Places</p>
                                <div className='seoplanbasic-btn'>
                                    <Button className='btn' buttonStyle='btn--orangel'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="seoplan-col2">
                                <h4 className="seoplanb">LOCAL</h4>
                                <h4 className="seorange">20</h4>
                                <p className="seorange">Keywords</p>
                                <p><FiCheckCircle className="check"/> On Page Optimization</p>
                                <p><FiCheckCircle className="check"/> 24/7 Keyword Analysis</p>
                                <p><FiCheckCircle className="check"/> Link Building</p>
                                <p><FiCheckCircle className="check"/> Google/Yahoo/Bing Places</p>
                                <div className='seoplanbasic-btn'>
                                    <Button className='btn' buttonStyle='btn--orangel'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="seoplan-col3">
                                <h4 className="seoplanw">PROFESSIONAL</h4>
                                <h4 className="seorangew">30</h4>
                                <p className="seorangew">Keywords</p>
                                <p><FiCheckCircle className="check"/> On Page Optimization</p>
                                <p><FiCheckCircle className="check"/> 24/7 Keyword Analysis</p>
                                <p><FiCheckCircle className="check"/> Link Building</p>
                                <p><FiCheckCircle className="check"/> Google/Yahoo/Bing Places</p>
                                <div className='seoplanstandred-btn'>
                                    <Button className='btn' buttonStyle='btn--owhitel' >GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div className="seoplan-col4">
                                <h4 className="seoplanb">ENTERPRISE</h4>
                                <h4 className="seorange">UNLIMITED</h4>
                                <p className="seorange">Keywords</p>
                                <p><FiCheckCircle className="check"/> On Page Optimization</p>
                                <p><FiCheckCircle className="check"/> 24/7 Keyword Analysis</p>
                                <p><FiCheckCircle className="check"/> Link Building</p>
                                <p><FiCheckCircle className="check"/> Google/Yahoo/Bing Places</p>
                                <div className='seoplanpremium-btn'>
                                    <Button className='btn' buttonStyle='btn--orangel'>GET IN TOUCH</Button>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
           </div>
    )
}

export default SEOplans
