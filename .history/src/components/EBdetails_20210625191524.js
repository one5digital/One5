import React from 'react';
import './EBdetails.css';

function EBdetails() {
    return (
        <div className="ebdetails">
            <div className="ebdetails">
                <div className="left-ebdetails">
                    <h1>WHAT WE <span style={{color: '#F04E31'}} >BELIEVE </span></h1>
                    <p>We believe that creating memorable experiences are the best way <br/>
                        to connect with your consumers. From dynamic web designs to <br/>
                        cutting-edge digital marketing strategies, we believe that the <br/>
                        custom solutions we create today will transcend the trends of <br/>
                        tomorrow. No matter your product or service, you have a story to tell.</p>
                </div>
            </div>
        </div>
    )
}

export default EBdetails
