import React from 'react';
import { Link } from 'react-router-dom';
 
function WDprojects() {
    return (
        <div>
            <div>
                <h1>FEATURED PROJECTS </h1>
            </div>
            <div className="pro-wrapper">
                <div className="prorow-1">
                    <ul className="prorow1-items">
                        <li>
                            <Link exact to="/Projects/EssentiallyBrands">
                            <img className="proimg" src="./essentiallybrands.png" alt="Loading..." /></Link>
                        </li>
                        <li>
                            <img className="proimg" src="./accord.png" alt="Loading..." />
                        </li>
                        <li>
                            <img className="proimg" src="./tiff.png" alt="Loading..." />
                        </li>
                        <li>
                            <img className="proimg" src="./aus.png" alt="Loading..." />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default WDprojects
