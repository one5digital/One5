import React from 'react'

function WDprojects() {
    return (
        <div>
            <div>
                <h1>FEATURED PROJECTS </h1>
                <p>We offer highly innovative design for the web and mobile.<br/>
                Right from user interfaces to complete campaigns, our approach to design stems <br/> 
                from a vision for the perfect marriage of aesthetics and functionality.</p>
            </div>
            <div className="pro-wrapper">
                <div className="prorow-1">
                    <ul className="prorow1-items">
                        <li>
                            <Link exact to="/Projects/EssentiallyBrands">
                            <img className="proimg" src="./essentiallybrands.png" /></Link>
                            <h6>PROJECT:<p>ESSENTIALLY BRANDS</p></h6>
                        </li>
                        <li>
                            <img className="proimg" src="./accord.png" />
                            <h6>PROJECT:<p>ACCORD RETAIL</p></h6>
                        </li>
                        <li>
                            <img className="proimg" src="./tiff.png" />
                            <h6>PROJECT:<p>TIFFANY SHEN</p></h6>
                        </li>
                        <li>
                            <img className="proimg" src="./aus.png" />
                            <h6>PROJECT:<p>AUSPORT</p></h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default WDprojects
