import React from 'react';
import './Ecomservice.css';

function Ecomservice() {
    return (
        <div className="ecservice">
        <div>
            <h1>OUR ECOMMERCE SERVICES</h1>
        </div>
        <div className="ecservice-wrapper">
            <ul className="ecservice-1">
                <h1 className="ecno">01</h1>
                <h6>ECOMMERCE WEBSITE <br/>DESIGN </h6>
                <p className="ec-desc">Our Ecommerce services bring <br/> value to your business by <br/> delivering the right custom <br/> solution that your business <br/> demands.
                </p>
            </ul>
            <ul className="ecservice-2">
                <h1 className="ecno">02</h1>
                <h6>INTEGRATION, UPGRADE, <br/>AND OPTIMIZATION</h6>
                <p className="ec-desc">With our experience in delivering <br/> what the client needs, we can <br/> build your Ecommerce website <br/> with no hassle.</p>
            </ul>
            <ul className="ecservice-3">
                <h1 className="ecno">03</h1>
                <h6>WEBSITE PERFORMANCE <br/>ANALYSIS </h6>
                <p className="ec-desc">We make sure your website <br/> displays the most important <br/> content as quickly as possible <br/> and don’t suffer unexpected <br/> downtimes.</p>
            </ul>
            <ul className="ecservice-4">
                <h1 className="ecno">04</h1>
                <h6>ONGOING SUPPORT AND <br/>MAINTENANCE </h6>
                <p className="ec-desc">To keep your eCommerce <br/> website running smoothly we <br/> offer complete maintenance <br/> and on-demand support to grow your business.</p>
            </ul>
        </div>
        <div className="ecdesc">
            <div className="ecleft-desc">
                <h1>WE OFFER DIFFERENT<br/> 
                ECOMMERCE SOLUTIONS</h1>
            </div>
            <div className="ecright-desc">
                <p className="ecproject-desc">We provide top e-commerce websites using the best <br/> e-commerce platforms keeping in mind <br/> your requirements and target audience.</p>     
            </div>
        </div>
        
    </div> 
    <div className="ec-logos">
    <ul className="eclogo">
        <li className="ecl1"><img src="./wordpress.png" alt="Loading..."/></li>
        <li className="ecl2"><img src="./shopify.png" alt="Loading..."/></li>
        <li className="ecl3"><img src="./magento.png" alt="Loading..."/></li>
        <li className="ecl4"><img src="./opencart.png" alt="Loading..."/></li>
    </ul>
</div>
    )
}

export default Ecomservice
