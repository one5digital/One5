import React from 'react';
import './WDservice.css';

function WDservice() {
    return (
        <div className="wdservice">
        <div>
            <h1>OUR WEBSITE DESIGN SERVICES</h1>
        </div>
        <div className="wdservice-wrapper">
            <ul className="wdservice-1">
                <img src= "./graphic-design.png" alt= "Loading..." />
                <h6>CUSTOM WEBSITE DESIGN</h6>
                <p className="wd-desc">A custom website design <br/> provides you with a trustworthy <br/> digital presence that meets your <br/> business needs in terms of <br/> quality, branding and usability.
                </p>
            </ul>
            <ul className="wdservice-2">
                <img src= "./responsive.png" alt= "Loading..." />
                <h6>FULLY RESPONSIVE</h6>
                <p className="wd-desc">Ensure your website adapts to all <br/> screen sizes and devices to <br/> increase your customer retention. </p>
            </ul>
            <ul className="wdservice-3">
                <img src= "./website.png" alt= "Loading..." />
                <h6>WEBSITE OPTIMIZATION</h6>
                <p className="wd-desc">Increase your leads and <br/> conversions with One5 Digital <br/> custom website design <br/> packages. </p>
            </ul>
            <ul className="wdservice-4">
                <img src= "./server.png" alt= "Loading..." />
                <h6>WEBSITE HOSTING</h6>
                <p className="wd-desc">Our web hosting services ensure <br/> high reliability and uptime, site <br/> security and improved SEO and <br/> online performance. </p>
            </ul>
        </div>
    </div>
    )
}

export default WDservice
