import React from 'react';
import './EBpro.css';

function EBpro() {
    return (
        <div className="ebpro">
            <div>
                <h1>MORE ABOUT PROJECT</h1>
                <p>Essentially Brands is an Australian owned and operated family business. Armed <br/> with 50 years of experience they specialise in delivering top quality 
                product for the <br/> Australian and export market.</p>
            </div>
            <div className="ebpro-slider">

            </div>
        </div>
    )
}

export default EBpro
