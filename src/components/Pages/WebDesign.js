import React from 'react';
import WDabout from '../WDabout';
import WDservice from '../WDservice';
import WDprojects from '../WDprojects';
import Getintouch from '../Getintouch';


function WebDesign() {
    return (
        <React.Fragment> 
            <WDabout />
            <WDservice />
            <WDprojects />
            <Getintouch />
        </React.Fragment>
    )
}

export default WebDesign
